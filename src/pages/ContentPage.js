import React from 'react';
import AboutMe from '../sections/content/AboutMe';
import Portfolio from '../sections/content/Portfolio';
import Skills from "../sections/content/Skills";
import ContactMe from "../sections/content/ContactMe";
import Sidebar from '../sections/sidebar/Sidebar';

const Content = () => {
  return (
    <div className='wrapper'>
      <Sidebar />
      <div className='content'>
        <AboutMe />
        <Portfolio />
        <Skills />
        <ContactMe />

      </div>
    </div>
  );
};

export default Content;
