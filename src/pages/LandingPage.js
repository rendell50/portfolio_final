import React, { Fragment } from 'react';
import { HashLink as Link } from 'react-router-hash-link';

const LandingPage = () => {
	return(
			<Fragment>
			<div className="body">
				<div className="landing-page-container">
				  	<div id="all-content">
				  		<div id="circles-container">
				  			<div id="circles">
				  				<img className="animated zoomIn"
				  				id="circle-black"
				  				src="assets/images/html.png"
				  				alt='black-circle'
				  				/>
				  				<div 
				  				id="left-aqua-container"
				  				className="container grow animated infinite pulse duration-5s delay-4s">
				  				<img 
				  				className="animated zoomIn image"
				  				id="left-aqua"
				  				src="assets/images/laravel.png"
				  				alt="aqua-circle"
				  				/>
				  				<div className="middle left-aqua-middle">
				  				<Link to="/content#aboutMe" smooth className="text text-dark left-aqua-text">
				  				About Me
				  				</Link>
				  				</div>
				  				</div>
				  				<img 
				  				className="animated zoomIn"
				  				id="blue-outline"
				  				src="assets/images/css.png"
				  				alt="blue circle"
				  				/>
				  				<img 
				  				 className="animated zoomIn"
				  				 id="left-purple-outline"
				  				 src="assets/images/react.png"
				  				 alt="purple circle"
				  				 />
				  				 <div id="yellow-circle-container" 
				  				 className="container grow">
				  				 <img 
				  				 className="animated zoomIn image"
				  				 src="assets/images/javascript.png"
				  				 alt="yellow circle"
				  				 />
				  				 <div className="middle yellow-triangle-middle"></div>
				  				 </div>

				  				 <div id="orange-circle-container" 
				  				 className="container grow">
				  				 <img 
				  				 className="animated zoomIn image"
				  				 src='assets/images/mysql.png'
				  				 alt="orange circle"
				  				 />
				  				 <div className='middle orange-circle-middle'></div>
				  				 </div>

				  				 <img 
				  				 className="animated zoomIn"
				  				 id='green-outline'
				  				 src="assets/images/node.png"
				  				 alt="green circle"
				  				 />
				  				 <img 
				  				 className="animated zoomIn"
				  				 id="grey-circle"
				  				 src="assets/images/graphql.png"
				  				 alt="grey circle"
				  				 />

				  				 <div id="right-red-container" className="container grow">
				  				 <img
				  				 className="animated zoomIn image"
				  				 src="assets/images/bootstrap.png"
				  				 alt="red circle"
				  				 />
				  				 <div className="middle right-red-middle"></div>
				  				 </div>
				  				 <img className="animated zoomIn"
				  				 id="right-black-outline"
				  				 src="assets/images/mongodb.png"
				  				 alt="black circle"
				  				 />
				  			</div>
				  		</div>
				  		<div id='greetings'>
              <div className='section-text'>
                <p className='animated fadeIn' id='text1'>
                  Hi, there!
                </p>
                <p className='animated fadeIn' id='text2'>
                  I'm Rendell
                </p>
                <p className='animated fadeIn' id='text3'>
                  And I'm a Web Developer
                </p>
              </div>
              <div className='socmed-icons animated fadeIn'>
                <a href='https://www.facebook.com/rendell50'>
                  <i className='fab fa-facebook-f grow'></i>
                </a>
                <a href='https://www.instagram.com/rendell50/'>
                  <i className='fab fa-instagram grow'></i>
                </a>
                <a href='https://www.linkedin.com/in/rendell-layus-2779b7142/'>
                  <i className='fab fa-linkedin-in grow'></i>
                </a>
                <a href='https://gitlab.com/rendell50'>
                  <i className='fab fa-gitlab grow'></i>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
				
			</Fragment>

		)
}

export default LandingPage;