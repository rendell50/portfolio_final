import React, { Fragment, useState } from 'react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem } from 'reactstrap';
import { HashLink as Link } from 'react-router-hash-link';

import './Sidebar.css';

const Sidebar = () =>{
	const [ collapsed, setCollapsed ] = useState(false);

	const toggleNavbar = () => setCollapsed(!collapsed);

	const phoneSize = window.matchMedia('(max-width: 360px)');

	const tabletSize = window.matchMedia('(max-width: 768px)');

	const handleCollapseMenu = () => {
		if (phoneSize.matches === true || tabletSize.matches === true) {
			setCollapsed(!collapsed);
		} else {
			setCollapsed(collapsed);
		}
	};

	return(
		<Fragment>

			<Navbar fixed="top" className="sidebar" light expand="md">
				<NavbarBrand href="#">
					<div className="sidebar-header">
						<img className="sidebar-logo grow" src="assets/images/r.gif"
						alt="logo-portfolio"
						/>
						</div>
				</NavbarBrand>
				<NavbarToggler
				onClick={toggleNavbar}
				className="mr-2 toggle-button"
				style={{ outline: "none"}}
				/>

				<Collapse isOpen={collapsed} navbar className="sidebar-collapse">
					<Nav className="list-unstyled sidebar-menu" navbar>
						<NavItem className="nav-item">
						 <Link to='content#aboutMe' smooth className="menu" onClick={handleCollapseMenu}>
						 <img
						 className="sidebar-menu-icon image"
						 src="assets/images/blue.png"
						 alt="blue circle"
						 />
						 <span className="text text-blue">About Me</span>
						 </Link>
						 </NavItem>
						 <NavItem className="nav-item">
						  <Link to='content#Portfolio' smooth className="menu" onClick={handleCollapseMenu}>
						  	<img
						  	 className="sidebar-menu-icon image"
						  	 src="assets/images/green.jpg"
						  	 alt="green circle"
						  	 />
						  	 <span className='text text-green'>Portfolio</span>
						  </Link>
						  </NavItem>
						  <NavItem className="nav-item">
						  <Link to="content#Skills" smooth className="menu" onClick={handleCollapseMenu}>
						  <img
						  className="sidebar-menu-icon image"
						  src="assets/images/yellow.jpg"
						  alt="yellow circle"
						  />
						  <span className="text text-orange">Skills</span>
						  </Link>
						   </NavItem>
						   <NavItem className="nav-item">
						   <Link to="content#contactMe" smooth className="menu" onClick={handleCollapseMenu}>
						   <img
						   className="sidebar-menu-icon image"
						   src="assets/images/red.png"
						   alt="red circle"
						   />
						   <span className="text text-red">Contact Me</span>
						   </Link>
						   </NavItem>
						</Nav>
						<Nav className="list-unstyled sidebar-socmed-icons">
						<NavItem>
							<a href='https://www.facebook.com/rendell50'>
							<i className='fab fa-facebook-f grow'></i>
							</a>
						</NavItem>
						<NavItem>
							<a href='https://www.instagram.com/rendell50/'>
							<i className='fab fa-instagram grow'></i>
							</a>
						</NavItem>
						<NavItem>
						<a href='https://www.linkedin.com/in/rendell-layus-2779b7142/'>
						<i className='fab fa-linkedin-in grow'></i>
						</a>
						</NavItem>
						<NavItem>
						<a href='https://gitlab.com/rendell50'>
						<i className='fab fa-gitlab grow'></i>
						</a>
						</NavItem>
						</Nav>
				</Collapse>
			</Navbar>
		</Fragment>

		)
}

export default Sidebar;
