import React, { Fragment } from 'react';

const AboutMe = () => {
	return(
		<Fragment>
		<section className="about-me" id="aboutMe">
		<div className="section-title-container">
			<h1>About Me</h1>
		</div>
		<div className='section-content'>
			<div className="section-text">
			<p>
			Hello! My name is Rendell Layus and I'm a Full Stack Web Developer from Imus City.
			</p>
			<p>
			My interest in the IT industry started during my capstone 2 project in Zuitt 
			where I realized that there are many things you can do when you can create a website.
			A lot more realizations happen in my life since I started the Bootcamp. If there is one thing
			you can't stop me from doing it, that is coding.
			</p>
			<p>
			When I'm not coding, I like to spend my time with my family and relatives in our provinces where 
			I can relax and enjoy every moment.
			</p>
			</div>
			<div className="profile-pic">
				<img className="grow" src="assets/images/profile.jpeg" alt="author business attire"
					/>
		</div>
		</div>

		</section>

		</Fragment>

		)
}

export default AboutMe;