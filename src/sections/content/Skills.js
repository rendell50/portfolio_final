import React, {Fragment} from 'react';

const Skills = () => {
    return (
        <Fragment>
            <section className="skills" id="Skills">
                <div className="section-title-container">
                    <h1>Skills</h1>
                </div>
                <div className="section-content">
                    <div className="toolkit">
                        <img src="assets/images/html.png" alt="html" />
                        <img src="assets/images/css.png" alt="css" />
                        <img src="assets/images/javascript.svg" alt="javascript" />
                        <img src="assets/images/bootstrap.svg" alt="bootstrap" />
                        <img src="assets/images/php.svg" alt="php" />
                        <img src="assets/images/laravel.svg" alt="laravel" />
                        <img src="assets/images/mysql.svg" alt="mysql" />
                        <img src="assets/images/heroku.svg" alt="heroku" />
                        <img src="assets/images/mongodb.svg" alt="mongodb" />
                        <img src="assets/images/express.svg" alt="express" />
                        <img src="assets/images/react.svg" alt="react" />
                        <img src="assets/images/nodejs.svg" alt="nodejs" />
                        <img src="assets/images/jwt.svg" alt="jwt" />
                    </div>
                </div>
            </section>
        </Fragment>
    )
}

export default Skills;